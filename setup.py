# exWx/setup.py
#
## python setup.py py2exe 
#
from distutils.core import setup
import matplotlib
#noinspection PyUnresolvedReferences
import py2exe

setup(
    windows = [
              {'script': "ascore_main_20.pyw",
              'icon_resources':[(0,'img//ascore48.ico')],
              'dest_base' : "ascore1.5",    
              'version' : "1.5",
              'company_name' : "JoaquinAbian",
              'copyright' : "No Copyrights",
              'name' : "Ascore"
              }
              ],
            
    options = {
              'py2exe': {
                        'packages' :    ['matplotlib', 'pytz'],
##                        'includes':     [],
                        'excludes':     ['EasyDialogs', 'PyQt4',
                                         'bsddb', 'curses', 'Pyrex',
                                         'email', 'testing', 'fft',
                                         '_gtkagg', '_tkagg', 'pywin.debugger',
                                         'pywin.debugger.dbgcon', 'pywin.dialogs',
                                         'Tkconstants','Tkinter', 'tcl'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk_pixbuf-2.0-0.dll',
                                         'libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll',
                                         'tk84.dll','tcl84.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1
                        }
              },
    zipfile = None,
    data_files = matplotlib.get_py2exe_datafiles()
                 
    )
