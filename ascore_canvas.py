#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ascore_main_20 (ascore_20)
10 de abril de 2009
"""
#
from matplotlib import use
use('WXAgg')
#
import wx
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure
#
#
class CanvasPanel(wx.Panel):
    """"""
    # noinspection PyArgumentList
    def __init__(self, parent, id_=-1, pos=wx.DefaultPosition,
                 size=(200, 250), xlabel='level', ylabel='score', tx='', ty=''):
        wx.Panel.__init__(self, parent, id_, pos, size, wx.RAISED_BORDER)
        #
        self.SetBackgroundColour("white")
        self.SetMinSize = (200, 100)
        self.figure = Figure()   # (x,y) en inches!!
        #
        self.axes = self.figure.add_subplot(111)
        self.xlabel = xlabel
        self.ylabel = ylabel
        #
        self.toolbar = None
        self.xtext = None
        self.ytext = None
        #
        self.canvas = FigureCanvas(self, -1, self.figure)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.add_toolbar(tx, ty)
        sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        self.SetSizer(sizer)
        self.Fit()
        #
        self.draw()

    def add_toolbar(self, tx, ty):
        """Add toolbar"""
        self.toolbar = NavigationToolbar2Wx(self.canvas)
        self.toolbar.SetToolBitmapSize(wx.Size(21, 25))
        self.toolbar.DeleteToolByPos(2)
        self.toolbar.DeleteToolByPos(1)
        xpos = 200
        xlabel = wx.StaticText(self.toolbar, -1, label=tx, pos=(xpos, 6),
                               size=(28, 15), style=wx.ALIGN_RIGHT)
        ylabel = wx.StaticText(self.toolbar, -1, label=ty, pos=(xpos+86, 6),
                               size=(28, 15), style=wx.ALIGN_RIGHT)
        #
        self.xtext = wx.TextCtrl(self.toolbar, -1, pos=(xpos+33, 3),
                                 size=(40, 22))
        self.ytext = wx.TextCtrl(self.toolbar, -1, pos=(xpos+115, 3),
                                 size=(45, 22))
        #
        xlabel.SetBackgroundColour("light gray")
        ylabel.SetBackgroundColour("light gray")
        #
        font = wx.Font(8, wx.FONTFAMILY_MODERN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
        self.xtext.SetFont(font)
        self.ytext.SetFont(font)
        #
        self.toolbar.Realize()
        self.toolbar.update()

    def draw(self):
        """"""
        self.axes.set_xlabel(self.xlabel)
        self.axes.set_ylabel(self.ylabel)
        #
        self.set_tick_size()
        self.canvas.draw()

    def set_tick_size(self):
        """"""
        tlx = list(self.axes.get_xticklabels())
        tlx.extend(list(self.axes.get_yticklabels()))
        for label in tlx:
            label.set_fontsize('xx-small')
