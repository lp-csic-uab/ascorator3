#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
macheator (ascore_20)
14 march 2009
"""
#from two spectra, experimental and expected,
# calculates matches at different levels
#
SECTOR_SIZE = 100
WINDOW = 0.3
MAX_LEVELS = 10
#
#
def get_sectors(spectrum, sz=SECTOR_SIZE, static=False):
    """spectrum must be sorted"""
    sectors = []
    init = 0
    #start initiates at 100 or at the minimum value available
    start = 100 if static else spectrum[0][0]
    end_sector = start + sz
    #end = spectrum[-1][0]
    for idx, val in enumerate(spectrum):
        #while mass below the range, move on (collect).
        if val[0] < end_sector:
            continue
        #if just passed the sector, save what you collected
        elif idx-init > 1:
            sectors.append(spectrum[init:idx])
            init = idx
        #after collection, jump to the next sector
        else:
            while val[0] > end_sector: end_sector += sz
                
    sectors.append(spectrum[init:])
    return sectors
#
#
def get_matches(sectors, expected, window=WINDOW,
                max_level=MAX_LEVELS, accumulated=False):
    """'expected' should be sorted to work"""
    matches = [0] * max_level
    for ion in expected:
        for sector in sectors:
            if ion > max(sector)[0] + window: continue
            if ion < min(sector)[0] - window: break
            for level in range(0, max_level):
                try:
                    mass = sector[level][0]
                except IndexError:
                    break
                if (mass-window) <= ion <= (mass+window):
                    print("level %i  match %.2f %.2f" % (level+1, ion, mass))
                    matches[level] += 1
                    break
                     
    if accumulated:
        full = []
        total = 0
        for hits in matches:
            total += hits
            full.append(total)
        return full
    else:   
        return matches
#
#
def order_sectors(sectors):
    new_sector = []
    for sector in sectors:
        sector = sorted(sector, reverse=True, key=lambda x:x[1])
        new_sector.append(sector)
    return new_sector
     
                
if __name__ == "__main__":
    
    from time import time
    
    repeat = 50
    
    expected = [351.10,  385.61, 450.13, 514.16,  636.82,  700.85,  701.20,
                765.37,  770.22, 799.88, 899.27, 1027.32, 1272.63, 1400.69,
               1529.73, 1598.75]

    exprmntl = [( 353.11,  11.2), ( 371.11,   9.9), ( 379.10,   6.4), ( 387.10,   4.1),
                ( 405.12,   4.0), ( 606.01,   9.6), ( 610.18,  24.0), ( 611.26,  16.7),
                ( 628.14,  11.6), ( 629.29,  10.0), ( 637.01,  27.4), ( 655.07,  15.0),
                ( 672.00,  38.4), ( 673.08,  10.6), ( 735.00,  34.1), ( 739.32,  16.6),
                ( 757.22,  50.8), (758.29,   14.8), ( 765.93,  12.4), ( 783.05,  19.7), 
                ( 880.93,  23.8), ( 886.23, 100.3), ( 887.24,  35.5), ( 898.95,  85.6),
                ( 929.06,  25.9), (1015.28, 217.6), (1016.23, 177.0), (1019.30,  87.0),
                (1027.82, 157.4), (1028.42, 237.3), (1077.02, 478.2), (1101.06,5977.5),
                (1101.89, 970.0), (1092.08,1352.0), (1092.92, 496.6), (1167.37,  68.9),
                (1168.36,  35.9), (1185.40, 480.0), (1186.38, 285.4), (1187.32,  71.1), 
                (1254.41,  30.0), (1272.39,  86.5), (1273.35,  50.1), (1284.16, 135.2),
                (1285.15,  61.8), (1382.59,  25.4), (1400.50, 146.2), (1401.55,  93.9),
                (1413.48,  32.6), (1414.30,  25.6), (1511.59,  24.3), (1512.47,  18.5),
                (1529.58, 107.0), (1530.57,  76.7), (1531.67,  23.1), (1542.25,  38.4),
                (1543.31,  15.1), (1544.33,  10.2), (1598.62,  44.0), (1599.73,  28.5), 
                (1937.40,   3.6), (1939.64,   1.8), (1959.39,   9.5), (1960.32,   4.3), 
                (1961.32,   1.6)]
                
    t0 = time()
    for i in range(repeat):
        x_sectors = get_sectors(exprmntl)
        ox_sectors = order_sectors(x_sectors)
        m1 = get_matches(ox_sectors, expected, window=WINDOW)
    
    t1 = time()

    #noinspection PyUnboundLocalVariable
    for item in ox_sectors: print([(int(a), int(b)) for a,b in item])
    #noinspection PyUnboundLocalVariable
    print(m1)
    print(t1-t0)

##[(353, 11), (371, 9), (379, 6), (387, 4), (405, 4)]
##[(637, 27), (610, 24), (611, 16), (628, 11), (629, 10), (606, 9)]
##[(672, 38), (735, 34), (739, 16), (655, 15), (673, 10)]
##[(757, 50), (783, 19), (758, 14), (765, 12)]
##[(886, 100), (898, 85), (887, 35), (929, 25), (880, 23)]
##[(1028, 237), (1015, 217), (1016, 177), (1027, 157), (1019, 87)]
##[(1101, 5977), (1092, 1352), (1101, 970), (1092, 496), (1077, 478)]
##[(1185, 480), (1186, 285), (1187, 71), (1167, 68), (1168, 35)]
##[(1284, 135), (1272, 86), (1285, 61), (1273, 50), (1254, 30)]
##[(1400, 146), (1401, 93), (1413, 32), (1414, 25), (1382, 25)]
##[(1529, 107), (1530, 76), (1542, 38), (1511, 24), (1531, 23), (1512, 18), (1543, 15),
## (1544, 10)]
##[(1598, 44), (1599, 28)]
##[(1937, 3), (1939, 1)]
##[(1959, 9), (1960, 4), (1961, 1)]
##[4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##0.0399
