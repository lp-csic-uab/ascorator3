#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ejecutor_13 (ascore_20)
10 de abril de 2009
"""
import os
from ejecutor_14 import load_spec, get_range_specific, get_level
from ejecutor_14 import get_all_ions, get_full_sec_scores, get_groups
from ejecutor_14 import get_spec_sec_scores
from combinator_03 import get_peptide_alternatives, codes
from fragmentor_02 import get_y_series, get_b_series
from macheator_04 import get_sectors, order_sectors, get_matches
from macheator_04 import SECTOR_SIZE
#
#
def _test(peptide, espectro, WIN=0.45, charge=2):
    ""
    #
    miexperimental = load_spec(espectro)
    start, end = get_range_specific(peptide)
    #
    taglist = get_peptide_alternatives(peptide[start:end+1])
    alternatives = [peptide[:start] + tag + peptide[end+1:] for tag in taglist]
    #
    data = get_all_ions(alternatives, start, end)
    sectors = get_sectors(miexperimental, SECTOR_SIZE, True)
    #
    osectors = order_sectors(sectors)
    #
    print "****sectores****"
    for sector in osectors: print ["%6.1f %4i" %(a,b) for a,b in sector]
    print ""
    #
    forsorting = get_full_sec_scores(data, osectors, WIN, charge)
    #
    sec_1, groups = get_groups(forsorting)
    scr_1 = data[sec_1]['p_scores']
    #
    resultado = [sec_1]
    #
    for group in groups:
        p_pos = group[0]
        for dataset in group[1]:
            sec_2 = dataset[0]
            faa = dataset[1]    #11
            laa = dataset[2]    #14
            sfaa = faa-start
            slaa = laa-start
            scr_2 = data[sec_2]['p_scores']
            level = get_level(scr_1, scr_2)
            
            resultado.extend([str(level), str(p_pos)])
            
##calcular con los iones especificos y el Ascore##
            dtascore = []
            for sec in (sec_1, sec_2):
                scr = get_spec_sec_scores(data, sec, sfaa, slaa,
                                          osectors, win=WIN, level=level)
                dtascore.append(scr)
    #
            Ascore = dtascore[0] - dtascore[1]
            resultado.append('%.3f' %Ascore)
    #
    return resultado
    
    
def run_xcel_list(fxls, dir_dtas, fout):
    "trabaja un archivo con columnas de secuencia, archivo mgf_name y ascore"
    
    if os.path.exists(fout):
        os.remove(fout)
        
    f = open(fout,'a')
    #
    for line in open(fxls):
        peptide, arch,  ascore = line.strip().split()
        print arch
        arch = os.path.join(dir_dtas,arch)
        try:
            datos =  _test(peptide, arch)
        except:
            datos = [arch]
        print >> fout, datos
    
    

if __name__ == "__main__":
    import psyco
    psyco.full()
    #
    WIN = 0.3
    #
    fexcel = "test/test_ascore_gygi.xls"
    dir_dtas = 'test/sp_dta'
    archivo = 'test/resultado'
    #
    #run_xcel_list(fxcel, dir_dtas, archivo)
    #
######################################################
    #
    peptide = "KLEKEEEEGIsQEuSEEEQ"
    espectro = "test/specId_616.mgf_name"
    #
    peptide = "EtPHSPGVEDAPIAK"
    espectro = "test/s-pxsp-2.1712.1712.2.dta"
    win = 0.4
    
    print _test(peptide, espectro, win, 2)
    
