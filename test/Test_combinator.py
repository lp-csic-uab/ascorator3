import unittest as ut
from combinator_03 import permutations

class TestCombinator(ut.TestCase):
    
    def setUp(self):
        self.seq = ['1','0']
        
    def test_permutations(self):
        self.assertEqual(list(permutations(self.seq)), [['1','0'],['0','1']])
        
if __name__ == '__main__':
    ut.main()
