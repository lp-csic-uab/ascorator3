def doit(main, listado):
    for idx, amac in enumerate(main):
        if amac not in "stuvy": continue
        #
        # -new- recoge secuencias que para la posicion del p-site en main
        #tengan un aminoacido no fosforilado
        new = []
        for sec in listado:
            if amac != sec[idx] and sec[idx].isupper():
                print idx, sec
                new.append(sec)
                
                
if __name__ == "__main__":
    main = "ABCDFGDHDGEsJFHEHSAWJtRHDSJW"
    listado = [ "ABCDFGDHDGESJFHEHSAWJtRHDSJW",
                "ABCDFGDHDGEsJFHEHSAWJtRHDSJW",
                "ABCDFGDHDGESJFHEHSAWJTRHDSJW",
                "ABCDFGDHDGESJFHEHSAWJtRHDSJW",
                "ABCDFGDHDGEsJFHEHSAWJTRHDSJW"
                ]