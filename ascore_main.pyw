#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ascore_main_20 (ascore_20)
10 april 2009
"""
#
from matplotlib import use
use('WXAgg')
#
import os
import wx
import _thread as thread
from matplotlib.artist import setp
from ascore_common import ICON, WELCOME
from ejecutor import load_spec, get_range_specific
from ejecutor import get_all_ions
from ejecutor import get_sequence_scores, get_groups
from ejecutor import get_level, get_spec_sec_scores
from combinator import get_peptide_alternatives
from ascorator import AscoreBaseFrame
from macheator import SECTOR_SIZE, get_sectors, order_sectors
from fragmentor import fake_the_tag
# from commons.warns import tell
from commons.iconic import Iconic
#
#
class MyFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window

    def OnDropFiles(self, x, y, filenames):
        self.window.notify(filenames)
#
#
# noinspection PyUnusedLocal
class AscoreFrame(AscoreBaseFrame, Iconic):
    """"""
    def __init__(self, title):
        AscoreBaseFrame.__init__(self, None, -1, title=title, size=(600, 300))
        Iconic.__init__(self, icon=ICON)
        self.canvas.canvas.mpl_connect('motion_notify_event', self.on_motion)

        dropper = MyFileDropTarget(self)
        self.tc_spf.SetDropTarget(dropper)

        self.Bind(wx.EVT_CLOSE, self.on_close_window)
        self.Bind(wx.EVT_BUTTON, self.on_bt_filein, self.bt_filein)
        self.Bind(wx.EVT_BUTTON, self.on_run, self.bt_run)

        self.start = None
        self.end = None
        self.data = None
        self.candidates = None
        self.ascores = []

        self.sequence = "KLEKEEEEGIsQEuSEEEQ"
        self.mgf_name = "test/specId_616.mgf"
        # self.sequence = 'R(737)DS(23)VLAASR'
        # self.mgf_name = "test/spectrum-4647_mod.mgf"
        self.mgf_dir = os.getcwd()
        self.tc_spf.SetLabel(self.mgf_name)
        self.tc_sec.SetLabel(self.sequence)
        self.tc_out.WriteText("\n".join(WELCOME))
        self._do_layout()

    def notify(self, files):
        """Informs when a drag and drop occurs."""
        if files[0].endswith('.mgf'):
            self.mgf_name = files[0]
            short_name = self.shorten_mgf_name()
            self.tc_spf.SetLabel(short_name)
            print('to be implemented')
    #
    def on_bt_filein(self, evt):
        """ dialog to open files"""
        #
        dialog = wx.FileDialog(None, defaultDir=self.mgf_dir)
        if dialog.ShowModal() == wx.ID_OK:
            self.mgf_name = dialog.GetPath()
            short_name = self.shorten_mgf_name()
            #
            self.tc_spf.SetLabel(short_name)
            self.mgf_dir = os.path.dirname(self.mgf_name)
            dialog.Destroy()
        else:
            dialog.Destroy()
    #
    def shorten_mgf_name(self):
        """"""
        new_name = ""
        if len(self.mgf_name) > 30:
            parts = self.mgf_name.split(os.path.sep)
            print(parts)
            name_size = 0
            name = []
            for item in reversed(parts):
                name_size += len(item)
                if name_size < 40:
                    name.append(item)
            new_name = ".../" + "/".join(reversed(name))
        else:
            new_name = self.mgf_name

        return new_name

    #
    def on_run(self, evt):
        """start run thread"""
        thread.start_new_thread(self.threaded_on_run, ())
    #
    # noinspection PyPep8Naming
    def threaded_on_run(self):
        """"""
        self.bt_run.SetBackgroundColour('grey')
        self.bt_run.Disable()
        #
        sequence = self.tc_sec.GetValue()
        charge = int(self.sc_ch.GetValue())
        mz_start = int(self.tc_mrg1.GetValue())
        mz_end = int(self.tc_mrg2.GetValue())
        error = float(self.tc_merr.GetValue())
        #
        if sequence:
            self.sequence = fake_the_tag(sequence)
        sequence = self.sequence
        #
        try:
            experimental = load_spec(self.mgf_name)
            self.tc_out.Clear()
            self.tc_out.WriteText("Calculating...\n")
        except IOError:
            self.bt_run.SetBackgroundColour('red')
            self.bt_run.Enable()
            self.tc_out.WriteText("Error Reading File %s\n" % self.mgf_name)
            return
        #
        self.start, self.end = get_range_specific(sequence)
        nterm_pep = sequence[:self.start]
        cterm_pep = sequence[self.end+1:]
        #
        tag_list = get_peptide_alternatives(sequence[self.start:self.end+1])
        alternatives = [nterm_pep + tag + cterm_pep for tag in tag_list]
        #
        self.data = get_all_ions(alternatives, self.start, self.end)
        #
        sectors = get_sectors(experimental, SECTOR_SIZE)
        ordered_sectors = order_sectors(sectors)
        #
        self.candidates = get_sequence_scores(self.data, ordered_sectors, error,
                                              charge=charge, m_start=mz_start,
                                              m_end=mz_end, poly_z=True)
        #
        sequence_1, groups = get_groups(self.candidates)
        score_1 = self.data[sequence_1]['p_scores']
        print("\nsec base= ", sequence_1)
        #
        self.ascores = []
        # groups -> [(idx, [(sequence, first, last),...]), ...]
        for p_site_info in groups:
            print('*********new group*********')
            for best_info in p_site_info[1]:
                sequence_2, start, end = best_info
                print('info best ', best_info)
                first_aa = start - self.start
                last_aa = end - self.start
                score_2 = self.data[sequence_2]['p_scores']
                level = get_level(score_1, score_2)
                print("level = ", level)
                
                basket = []
                if level:
                    for sec in (sequence_1, sequence_2):
                        score = get_spec_sec_scores(self.data, sec, first_aa, last_aa,
                                                    ordered_sectors, win=error, level=level,
                                                    charge=charge, poly_z=True)
                        basket.append(score)
                    #
                    Ascore = basket[0] - basket[1]
                else:
                    Ascore = 0

                self.ascores.append((sequence_1, p_site_info[0], sequence_2, level, Ascore))
                print("Ascore = ", Ascore)
        
        self.print_scores()
        #
        self.lb_sec.SetLabel("%s" % sequence_1)
        widgets = (self.lb_asc_1, self.lb_asc_2, self.lb_asc_3)
        for item, widget in zip(self.ascores, widgets):
            widget.SetLabel("%.3f" % item[-1])
        #
        self.plot_scores()
        self.bt_run.SetBackgroundColour('red')
        self.bt_run.Enable()
            
    def print_scores(self):
        """"""
        text = []
        for sequence in self.data:
            text.append(sequence)
            lst = ["%.4f" % item for item in self.data[sequence]['p_scores']]
            text.extend(lst)
        #
        text.append("\n")
        #  
        for item in self.candidates:
            text.append("%.3f  %s" % item)
        #
        form = "\n%s %i\n%s\nlevel=%i ascore=%.2f"
        for sequence_1, site_index, sequence_2, level, ascore in self.ascores:
            text.append(form % (sequence_1, site_index,
                                sequence_2, level, ascore))
        #
        self.tc_out.WriteText("\n".join(text))    
    
    def plot_scores(self):
        """"""
        xticks = None
        axes = self.canvas.axes
        axes.clear()
        for sequence in self.data:
            to_plot = self.data[sequence]['p_scores']
            xticks = range(1, len(to_plot)+1)
            axes.plot(xticks, to_plot, label=sequence[self.start:self.end+1])
        axes.legend()
        legend = axes.get_legend()
        txt = legend.get_texts()
        lns = legend.get_lines()
        setp(txt, fontsize='x-small')
        setp(lns, linewidth=3)
        print('-->', self.ascores)
        for data_level in self.ascores:
            # data_level (u'KLEKGISQEusEQ', 13, u'KLEKGIuQESsEQ', 2, 38.1555116)
            level = data_level[3]
            axes.axvline(level, linestyle=':')
        axes.set_xlim(xticks[0]-.5, xticks[-1]+.5)
        axes.set_xticks(xticks)
        self.canvas.draw()
       
    def on_motion(self, evt):
        """"""
        if evt.inaxes:
            self.canvas.xtext.SetValue('%.1f' % evt.xdata)
            self.canvas.ytext.SetValue('%.1f' % evt.ydata)
    
    def on_close_window(self, evt):
        self.Destroy()
        

if __name__ == '__main__':
    
    # import psyco
    # psyco.log()
    # psyco.profile()
    # psyco.full()
    
    app = wx.App()
    AscoreFrame(title="yo").Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
