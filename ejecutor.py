#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ejecutor_13 (ascore_20)
10 april 2009
"""
import math
from pylab import plot, show, hold
from time import time
from scipy.stats import binom
from combinator import get_peptide_alternatives, CODES
from fragmentor import get_y_series, get_b_series
from macheator import get_sectors, order_sectors, get_matches
from macheator import SECTOR_SIZE
#
# this must be documented
WEIGHTS = [0.52, 0.75, 1, 1, 1, 1, 0.75, 0.5, 0.25, 0.25]
#WEIGHTS = [1]*10
SUM_WEIGHTS = sum(WEIGHTS) #7.02
#
def get_weighted_score(scores):
    """weighted score for a score list"""
    val = sum(score*weight for score, weight in zip(scores, WEIGHTS)) / SUM_WEIGHTS
    return val
#
def load_spec(filename):
    """loads a dta file"""
    spectrum = []
    for line in open(filename):
        if line[0].isdigit():
            aln = line.strip().split()
            pair = (float(aln[0]), float(aln[1]))
            spectrum.append(pair)
    spectrum.sort()
    return spectrum
#
#
def get_range_specific(sequence):
    """Indexes for p-site specific zone.

    starts at 0 <-> are list indexes
    KLEKEEEEGIsQEuSEEEQ -> start=10 end=14

    """
    new = True
    start = end = 0
    for idx, amino_acid in enumerate(sequence):
        if amino_acid in CODES:
            if new:
                start = idx
                new = False
            else:
                end = idx
    return start, end
#
#
#noinspection PyUnboundLocalVariable
def get_all_ions(alternatives, start, end):
    """Yield  y, b mono-charged fragments covering a section of the sequence.

    Needs documentation

    """
    data = {}
    is_first = True
    #
    for sequence in alternatives:
        # alternative sequences have always the same sequence outside the
        # variable p-site defined range
        if is_first:
            y_series = get_y_series(sequence)
            b_series = get_b_series(sequence)
            y_series.reverse()
            y_commons = y_series[:start+1] + y_series[end+1:]
            b_commons = b_series[:start] + b_series[end:]
            y_special = y_series[start+1:end+1]
            b_special = b_series[start:end]
            common_ions = y_commons + b_commons
            #print common_ions
            special_ions = y_special + b_special
            start_y_block = y_series[end+1] - 19.0176
            start_b_block = b_series[start-1] - 1.007
            is_first = False
        else:
            y_series = get_y_series(sequence[start+1:end+1])
            b_series = get_b_series(sequence[start:end])
            y_series.reverse()
            y_special = [item + start_y_block for item in y_series]
            b_special = [item + start_b_block for item in b_series]
            special_ions = y_special + b_special
            
        data[sequence] = {'ct':common_ions, 'st':special_ions,
                          'sy1':y_special, 'sb1':b_special
                         }
    #
    return data
#
#
def calc_poly_z(ions, charge, full=False):
    """Build the spectrum multi-charged ion collection

    if full=False:
    - precursor charge 2 -> considers only mono-charged
    - precursor charge z -> considers charges z-1 to 1

    """
    if full: charge += 1
    ions_base = ions[:]
    for z in range(2, charge):
        z2 = z - 1
        ions_z = [(mass + z2)/z for mass in ions]
        ions_base.extend(ions_z)
    return ions_base
#
#
def filter_mass(ions, mass_start, mass_end):
    """return ions in a mass range"""
    return [ion for ion in ions if mass_end >= ion >= mass_start]
#
#
def get_spec_sec_scores(data, sec, start, end, sectors, win, level,
                        charge=2, poly_z=True):
    """Calculate second score for peptide sec[start:end].

    Calculations for a fixed level.

    """
    prob = level/100.
    ions = None
    y = data[sec]['sy1']
    b = data[sec]['sb1']
    y_ions = y[start-1:end-1]
    b_ions = b[start-1:end-1]
    
##    print sec
##    print start, end
##    print 'y ', y
##    print 'sy ', y_ions
##    print
##    print 'b ', b
##    print 'sb ', b_ions
    
    t_ions = y_ions + b_ions
    if poly_z: ions = calc_poly_z(t_ions, charge)

    trials = len(ions)
    matches = get_matches(sectors, ions, window=win,
                            max_level=level, accumulated=True)
    print("%i matches for %i trials" %(matches[-1],trials))
    return get_score(matches[-1], trials, prob)
#
#
#noinspection PyUnboundLocalVariable
def get_sequence_scores(sequence_data, sectors, win, charge=2,
                           m_start=100, m_end=2500, poly_z=True):
    """calculate first score.
    """
    candidates = []
    is_first = True
    for sequence in sequence_data:
        print(sequence)
        data = sequence_data[sequence]
        if is_first:
            c_ions = data['ct']
            if poly_z: c_ions = calc_poly_z(c_ions, charge)
            c_ions = filter_mass(c_ions, m_start, m_end)
            c_matches = get_matches(sectors, c_ions,
                                    window=win, accumulated=True)
            is_first = False
        s_ions = data['st']
        if poly_z: s_ions = calc_poly_z(s_ions, charge)
        s_ions = filter_mass(s_ions, m_start, m_end)
        print(['%.1f' %x for x in c_ions])
        print(['%.1f' %x for x in s_ions])
        s_matches = get_matches(sectors, s_ions,
                                    window=win, accumulated=True)
        total_matches = [i+j for i, j in zip(c_matches, s_matches)]
        trials = len(c_ions) + len(s_ions)
        #
        print("total matches", total_matches)
        print("trials", trials)
        #
        level_scores = []
        for idx, hits in enumerate(total_matches):
            prob = (idx + 1.) / 100
            score = get_score(hits, trials, prob)
            #
            level_scores.append(score)
        #
        total_score = get_weighted_score(level_scores)
        print('total score ', total_score)
        data['p_scores'] = level_scores
        data['p_matches'] = total_matches
        data['p_score'] = total_score
        candidates.append((total_score, sequence))
              
    return candidates
#
#
def get_groups(candidates):
    """Select sequences with alternative p-sites to compare with each other.

    From the list
    candidates -> [(avg_p_score, sequence),....]
    determines who compares with who to calculate level, specific ions and ascore.

    returns ->   [(p-site, [(p_alternative, start, end),...]),...]

                                 0        9       15
    KLEKEEEEGISQEusEEEQ [(13, [('KLEKEEEEGI uQESs EEEQ', 11, 14)]),
                         (14, [('KLEKEEEEGI sQEuS EEEQ', 11, 15)])
                        ]

    """
    print("***** get_groups *******")
    #
    candidates.sort(reverse=True)
    #
    # -main- A unique first alternative is taken
    #  even if there are two with equal score.
    main = candidates[0][1]
    sequence_scores = dict([(sequence, score) for score, sequence in candidates])
    #
    # -counterparts- stores sequences that only differ from the best (main) 
    #  by the position of a single p-site <==> (differences ==  2)
    counterparts = []
    for candidate in candidates[1:]:
        #print item
        differences = 0
        sequence = candidate[1]
        for aa1, aa2 in zip(main, sequence):
            if aa1 != aa2: differences += 1
        if differences == 2:
            counterparts.append(sequence)
    #
    # -groups- stores data for the best main partner for each p-site in main
    groups = []
    for idx, amino_acid in enumerate(main):
        if amino_acid not in "stuvy": continue
        #
        # -peers- collects sequences that for the position of the p-site in main
        #  have a non phosphorylated amino acid.
        peers = []
        for sequence in counterparts:
            if amino_acid != sequence[idx] and sequence[idx].isupper():
                print(idx, sequence)
                peers.append(sequence)
        #
        # -best_sequences- takes the sequence of higher score in peers
        # takes more if all have the same score.
        max_score = 0
        best_sequences = []
        for sequence in peers:
            peer_score = sequence_scores[sequence]
            if peer_score > max_score:
                max_score = peer_score
                best_sequences = [sequence]
            elif peer_score == max_score:
                best_sequences.append(sequence)
        #
        # -best_info- rebuild best_sequences in a list of tuples containing
        # the positions of the alternative amino acids used to generate
        # the fragments for the second score comparison.
        best_info = []
        for sequence in best_sequences:
            idx2 = 0
            init = True
            for aa1, aa2 in zip(sequence, main):
                #counts start at 1 -> it is aa position
                idx2 += 1
                if aa1 != aa2:
                    if init:
                        first = idx2
                        init = False
                    else:
                        last = idx2
            #noinspection PyUnboundLocalVariable
            best_info.append((sequence, first, last))
        groups.append((idx, best_info))
    
    return main, groups
#
#
def get_level(scores_1, scores_2):
    """Get index of maximal difference between numbers in two lists.

    From two series of numbers, gives the first index with higher difference.
    index (level) start at 1.
    scores_1 must be the best candidate.
    [2,3,9,9] - [1,4,6,8] -> level = 3

    """
    value = idx = level = 0
    for score_1, score_2 in zip(scores_1, scores_2):
        dif = score_1 - score_2
        idx += 1
        if dif > value:
            value = dif
            level = idx
    return level
#
#
def get_score(hits, trials, prob):
    """Calculates accumulated binomial and score

     - binom.pmf(hits, trials, prob)-

    """
    cumulative_binomial = sum(binom.pmf(range(hits, trials+1), trials, prob))
    #
    #noinspection PyTypeChecker
    return -10 * math.log(cumulative_binomial, 10)
#
#
def _test(sequence, spectrum_file, WIN=0.3):
    """"""
    t0 = time()
    #
    spectrum = load_spec(spectrum_file)
    start, end = get_range_specific(sequence)
    print(start, end)
    tag_list = get_peptide_alternatives(sequence[start:end+1])
    alternatives = [sequence[:start] + tag + sequence[end+1:] for tag in tag_list]
    print(alternatives)
    #
    t1 = time()
    #
    data = get_all_ions(alternatives, start, end)
    sectors = get_sectors(spectrum, SECTOR_SIZE)
    #
    ordered_sectors = order_sectors(sectors)
    #
    t2 = time()
    #
    candidates = get_sequence_scores(data, ordered_sectors, win=WIN)
    #
    t3 = time()
    #
    sequence_1, groups = get_groups(candidates)
    #
    t4 = time()
    #
    for attr in ('p_matches', 'p_scores', 'p_score'):
        print('*** ', attr, ' ***')
        for sequence in data:
            attr_values = data[sequence][attr]
            try:
                item = ["%i" % x for x in attr_values]
            except TypeError:
                item = '%.2f' % attr_values
            print(sequence, item)
         
    print('groups ', groups)
    #
    t5 = time()
    #
    score_1 = data[sequence_1]['p_scores']
    for p_site_info in groups:
        print('*********new group*********')
        for best_info in p_site_info[1]:
            print('data_set ', best_info)
            sequence_2 = best_info[0]
            first_aa = best_info[1]    #11
            last_aa = best_info[2]    #14
            first_special = first_aa - start
            last_special = last_aa - start
            score_2 = data[sequence_2]['p_scores']
            level = get_level(score_1, score_2)
            print("%s <--> %s    level %i" %(sequence_1, sequence_2, level))
    
##calculate with specific ions and Ascore##
            basket = []
            for sequence in (sequence_1, sequence_2):
                score = get_spec_sec_scores(data, sequence,
                                            first_special, last_special,
                                            ordered_sectors, win=WIN, level=level)
                basket.append(score)
    #
            Ascore = basket[0] - basket[1]
            print("Ascore = ", Ascore)
    #
    t6 = time()
    #
    print(t1-t0)
    print(t2-t1)
    print(t3-t2)
    print(t4-t3)
    print(t5-t4)
    print(t6-t5)
    #
##    
    hold(True)
    for sequence in data:
        y = data[sequence]['p_scores']
        x = range(1, len(y)+1)
        plot(x, y)                           #plot full peptide scores
    show()
    
    hold(True)
    for sequence in data:
        y = data[sequence]['p_matches']
        x = range(1, len(y)+1)
        plot(x, y)                           #plot full peptide matches
    show()    
    input()
    

if __name__ == "__main__":
    #import psyco
    #psyco.full()
    #
    WIN = 0.3
    peptide = "KLEKEEEEGIsQEuSEEEQ"
    spectrum_file = "test/specId_616.mgf_name"
    
##    sequence = "TQSTFEGFPQsPLQIPVSR"
##    spectrum = "test/sp_dta/s-pxsp-2.4097.4097.2.dta"

    peptide = "ETPHsPGVEDAPIAK"
    spectrum_file = "test/s-pxsp-2.1712.1712.2.dta"

    _test(peptide, spectrum_file)
    
    
##    sequence = "NsVPQRPGPPAsPASDPsR"   #level=7, score=31.3, asc1=24.71, asc2=11.56, asc3=27.05
##    spectrum = "test/s-pxsp-2.4364.4364.2.dta"
##    sequence = "ETPHsPGVEDAPIAK"       #level=5, score=70.68	asc1=13.38	(ETPHsPGVEDAPIAK)
##    spectrum = "test/s-pxsp-2.1712.1712.2.dta"
##    sequence = "AGGPTTPLsPTR"
##    spectrum = "test/s-pxsp-2.1643.1643.2.dta"
    #
    
    
##10 14
##['KLEKEEEEGIuQESsEEEQ', 'KLEKEEEEGIuQEsSEEEQ', 'KLEKEEEEGIsQEuSEEEQ',
##'KLEKEEEEGISQEsuEEEQ', 'KLEKEEEEGISQEusEEEQ', 'KLEKEEEEGIsQESuEEEQ']
##KLEKEEEEGIuQESsEEEQ
##total matches [6, 10, 15, 15, 19, 21, 22, 25, 25, 25]
##trials 74
##KLEKEEEEGISQEusEEEQ
##total matches [8, 13, 18, 18, 20, 22, 24, 27, 27, 27]
##trials 74
##KLEKEEEEGIsQEuSEEEQ
##total matches [5, 9, 13, 13, 15, 17, 18, 20, 20, 20]
##trials 74
##KLEKEEEEGIuQEsSEEEQ
##total matches [5, 9, 14, 14, 18, 20, 21, 23, 23, 23]
##trials 74
##KLEKEEEEGISQEsuEEEQ
##total matches [8, 13, 18, 18, 20, 22, 24, 25, 25, 25]
##trials 74
##KLEKEEEEGIsQESuEEEQ
##total matches [6, 10, 14, 14, 16, 18, 19, 20, 20, 20]
##trials 74
##13 KLEKEEEEGIuQESsEEEQ
##14 KLEKEEEEGIsQEuSEEEQ
##***  p_matches  ***
##KLEKEEEEGIuQESsEEEQ ['6', '10', '15', '15', '19', '21', '22', '25', '25', '25']
##KLEKEEEEGISQEusEEEQ ['8', '13', '18', '18', '20', '22', '24', '27', '27', '27']
##KLEKEEEEGIsQEuSEEEQ ['5', '9', '13', '13', '15', '17', '18', '20', '20', '20']
##KLEKEEEEGIuQEsSEEEQ ['5', '9', '14', '14', '18', '20', '21', '23', '23', '23']
##KLEKEEEEGISQEsuEEEQ ['8', '13', '18', '18', '20', '22', '24', '25', '25', '25']
##KLEKEEEEGIsQESuEEEQ ['6', '10', '14', '14', '16', '18', '19', '20', '20', '20']
##***  p_scores  ***
##KLEKEEEEGIuQESsEEEQ ['39', '56', '83', '66', '85', '88', '84', '95', '85', '75']
##KLEKEEEEGISQEusEEEQ ['60', '85', '112', '92', '93', '96', '99', '111', '100', '89']
##KLEKEEEEGIsQEuSEEEQ ['30', '47', '65', '51', '54', '58', '55', '60', '52', '45']
##KLEKEEEEGIuQEsSEEEQ ['30', '47', '73', '58', '77', '80', '76', '80', '71', '63']
##KLEKEEEEGISQEsuEEEQ ['60', '85', '112', '92', '93', '96', '99', '95', '85', '75']
##KLEKEEEEGIsQESuEEEQ ['39', '56', '73', '58', '61', '65', '62', '60', '52', '45']
##***  p_score  ***
##KLEKEEEEGIuQESsEEEQ 76.63
##KLEKEEEEGISQEusEEEQ 95.35
##KLEKEEEEGIsQEuSEEEQ 53.83
##KLEKEEEEGIuQEsSEEEQ 67.46
##KLEKEEEEGISQEsuEEEQ 93.18
##KLEKEEEEGIsQESuEEEQ 60.52
##groups  [(13, [('KLEKEEEEGIuQESsEEEQ', 11, 14)]), (14, [('KLEKEEEEGIsQEuSEEEQ', 11, 15)])]
##*********new group*********
##dataset  ('KLEKEEEEGIuQESsEEEQ', 11, 14)
##KLEKEEEEGISQEusEEEQ KLEKEEEEGIuQESsEEEQ 3
##matches  [2, 3, 4]
##matches  [0, 0, 1]
##Ascore =  29.6694677505
##*********new group*********
##dataset  ('KLEKEEEEGIsQEuSEEEQ', 11, 15)
##KLEKEEEEGISQEusEEEQ KLEKEEEEGIsQEuSEEEQ 8
##matches  [3, 4, 5, 5, 5, 5, 6, 8]
##matches  [0, 0, 0, 0, 0, 0, 0, 1]
##Ascore =  47.8831443228
##   
##0.0199999809265
##0.00999999046326
##0.170000076294
##0.00999999046326
##0.0309998989105
##0.0299999713898


##OLD CALCULATIONS
##Ascore =  19.3692496882
##0.0199
##0.00999999046326
##6.359