#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
combinator_02 (ascore_20)
9 march 2009

# XXXSXuXXTXXXsXXXYX     a peptide sequence
#    S u  T   s   Y      with some modified sites
#    0 2  0   1   0      to whom a key is assigned
#
#    20010              and all combinations determined
#    02010
#    00210
#    -----
#                       removing later those forbidden (2 for Y)
# The original amino acids are reassigned to each combination
# and the patron injected again in the original sequence
# That is!
"""
#
CODES = dict(S = ('0', 'S'), T = ('0', 'T'), Y = ('0', 'Y'),
             s = ('1', 'S'), t = ('1', 'T'), y = ('1', 'Y'),
             u = ('2', 'S'), v = ('2', 'T'))

KEYS = dict(S = ['S', 's', 'u'],
            T = ['T', 't', 'v'],
            Y = ['Y', 'y'])
#
#
def get_players(sequence):
    """"""
    return [CODES[item][0] for item in sequence if item in CODES]
#
#
def permutations(L):
    """"""
    if len(L) <= 1:
        yield L
    else:
        a = [L.pop(0)]
        for p in permutations(L):
            for i in range(len(p)+1):
                yield p[:i] + a + p[i:]
#
#
def get_permutations(data):
    """from a code
     ['0', '2', '0', '1', '0']
    returns the collection of permutations
     set(['00210', '02001', '20001', ... ,'20100'])
    """
    permuton = list(data)
    permulation = permutations(permuton)
    collection = [''.join(permulante) for permulante in permulation]
    return set(collection)
#
#
def get_combinations_from_codeset(sequence, codeset):
    """Generates possible sequences"""
    alternatives = []
    
    for code in codeset:
        amino_acids = list(sequence)
        #print code
        i = 0
        for idx, amino_acid in enumerate(amino_acids):
            if amino_acid in CODES:
                key = CODES[amino_acid][1]
                code_number = int(code[i])
                i += 1  
                try:
                    amino_acids[idx] = KEYS[key][code_number]
                except IndexError:
                    break
        #
        alternatives.append("".join(amino_acids))
    
    return alternatives
#
#
def get_peptide_alternatives(sequence):
    """"""
    code = get_players(sequence)
    codeset = get_permutations(code)
    return get_combinations_from_codeset(sequence, codeset)
        
        
if __name__ == "__main__":
    
    peptide = "XXXSXuXXTXXXsXXXYX"
    code = get_players(peptide)
    codeset = get_permutations(code)
    
    print (code)                  # ['0', '2', '0', '1', '0']
    print (codeset)               # set(['00210', '02001', '20001', ... ,'20100'])
    print (get_combinations_from_codeset(peptide, codeset))
    
##['0', '2', '0', '1', '0']
##set(['00210', '02001', '20001', '10200', '10002', '21000', '01002', '02100',
## '02010', '00021', '12000', '20010', '00201', '10020', '01020', '00102',
## '00120', '00012', '01200', '20100'])
##['XXXSXSXXvXXXsXXXYX', 'XXXSXuXXTXXXSXXXyX', 'XXXuXSXXTXXXSXXXyX',
## 'XXXsXSXXvXXXSXXXYX', 'XXXuXsXXTXXXSXXXYX', 'XXXSXuXXtXXXSXXXYX',
## 'XXXSXuXXTXXXsXXXYX', 'XXXSXSXXTXXXuXXXyX', 'XXXsXuXXTXXXSXXXYX',
## 'XXXuXSXXTXXXsXXXYX', 'XXXSXSXXvXXXSXXXyX', 'XXXsXSXXTXXXuXXXYX',
## 'XXXSXsXXTXXXuXXXYX', 'XXXSXSXXtXXXuXXXYX', 'XXXSXsXXvXXXSXXXYX',
## 'XXXuXSXXtXXXSXXXYX']
    