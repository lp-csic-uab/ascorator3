---

**WARNING!**: This is the *Old* source-code repository for Ascorator3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/ascorator3/) located at https://sourceforge.net/p/lp-csic-uab/ascorator3/**  

---  
  
  
# Ascorator3 program

Python 3 version of ascorator  


---

**WARNING!**: This is the *Old* source-code repository for Ascorator3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/ascorator3/) located at https://sourceforge.net/p/lp-csic-uab/ascorator3/**  

---  
  