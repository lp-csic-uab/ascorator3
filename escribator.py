#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
escribator (ascore_20)
14  march 2009

reads from an xls file the corresponding sequence and file from columns cs and cf
reads the file and determines Ascore
Generates an excel document with data cs, cf, ca(score)

##class Escribator():
##    def __init__(self, None, flin):
##        wb = xlrd.open_workbook(flin)
"""
import xlrd
import xlwt



#noinspection PyUnusedLocal
def xread(fle, sht='Hoja1', cs=0, cf=1, rs=0, re=10):
    """"""
    wbk = xlrd.open_workbook(fle)
    sht = wbk.sheet_by_name(sht)
    final = min(re, sht.nrows)
    for row in range(rs, final):
        linea = sht.row_values(row)
        yield linea
#
#
#noinspection PyUnusedLocal
def process(linea, cs=0, cf=1):
    """Add a column"""
    linea.append(linea[cs]*2)  #proceso
    return linea
#
#
class Writer():
    def __init__(self, flout):
        self.flout = flout
        self.idx = 0
        self.wb = xlwt.Workbook()
        self.wsht = self.wb.add_sheet('out')
    
    def write(self, line):
        row = self.wsht.row(self.idx)
        for col, value in enumerate(line):
            row.write(col, value)
        self.idx += 1
        
    def close(self):
        self.wb.save(self.flout)


if __name__ == "__main__":
    
    flin = 'C:/test.xls'
    it = xread(flin)
    wtr = Writer('C:/testout.xls')
    
    for linea in it:
        newline = process(linea)
        wtr.write(newline)
    
    wtr.close()
        