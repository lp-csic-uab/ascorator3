__author__ = 'joaquin'

import unittest
from ejecutor import get_level, get_groups
from macheator import order_sectors
from fragmentor import fake_the_tag

class MyTestCase(unittest.TestCase):
    def test_get_level(self):
        #             scr_1             scr_2           level
        scores = [((3, 4, 5, 6),      (2, 3, 4, 5),       1),
                  ([2, 3, 9, 9],      [1, 4, 6, 8],       3),
                  ([2, 3, 9, 9, 15],  [1, 4, 6, 8, 9],    5),
                  ([2, 3, 4, 7, 8, 9],[1, 2, 3, 4, 5, 6], 4),
                  #this should not occur...
                  ([1, 2, 3, 4, 5, 6],[2, 3, 4, 7, 8, 9], 0),
                 ]

        for scr_1, scr_2, level in scores:
            self.assertEqual(get_level(scr_1, scr_2), level)

    def test_get_groups(self):
        """From the list
        candidates -> [(avg_p_score, sequence),....]
        determines who compares with who to calculate level, specific ions and ascore.

        returns ->   [(p-site, [(p_alternative, start, end),...]),...]
        KLEKEEEEGISQEusEEEQ [
        (13, [('KLEKEEEEGIuQESsEEEQ', 11, 14)]),
        (14, [('KLEKEEEEGIsQEuSEEEQ', 11, 15)])
        """
        candidates1 = [(23, 'KLGISQESsEEEQ'),  #8
                      (13, 'KLGIsQESSEEEQ'),  #4
                      (53, 'KLGISQEsSEEEQ'),  #7
                     ]
        #                    7                        8
        expected1 = ('KLGISQEsSEEEQ', [(7, [('KLGISQESsEEEQ', 8, 9)])])

        candidates2 = [(20, 'KLEKEEEEGIuQESsEEEQ'),
                       (30, 'KLEKEEEEGIsQEuSEEEQ'),
                       (50, 'KLEKEEEEGISQEusEEEQ')
                      ]
                                                      #       10
        expected2 = ('KLEKEEEEGISQEusEEEQ', [(13, [('KLEKEEEEGIuQESsEEEQ', 11, 14)]),
                                             (14, [('KLEKEEEEGIsQEuSEEEQ', 11, 15)])
                                            ]
                    )
        for candidates, expected in [(candidates1, expected1),
                                     (candidates2, expected2)]:
            result = get_groups(candidates)
            self.assertEquals(result, expected)

    def test_order_sectors(self):
        sectors =  [[(1, 5), (2, 3), (4, 2), (5, 1), (3, 4)]]
        expected = [[(1, 5), (3, 4), (2, 3), (4, 2), (5, 1)]]
        result = order_sectors(sectors)
        self.assertEquals(result, expected)

    def test_fake_the_tag(self):
        tags = ['A(737)GSDFGS(21)HJ',
                'A(737)GS(737)DFGS(21)HJ',
                'A(214)GSDFGSHJ',
                'A(214, 23)GSDFGSHJ',
                'A(214)GS(21)DFGS(23)HJ',
                ]
        expected =  ['oGSDFGsHJ',
                     'oGoDFGsHJ',       #wrong result (two o from different aa)
                     'oGSDFGSHJ',
                     'oGSDFGSHJ',       #wrong result (p-site is lost)
                     'oGsDFGuHJ',
                     ]
        for tag, value in zip(tags, expected):
            result = fake_the_tag(tag)
            self.assertEquals(result, value)

if __name__ == '__main__':
    unittest.main()
