#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
fragmentor_02 (ascore_20)
10 march 2009
#
From a string of amino acids yields a dictionary.
KEYS are the fragment type symbol ('B' or 'Y').
For each type a list is stored with the mass of the different fragments
 PEPTIDE -> {'B':[p, pe, pep, pept, ..., peptide], 'Y':[e, de, ide, ...]}

mass_mod = (214=> 144.102063,      #itraq
            737=> 229.162932,      #tmt

"""

from commons.mass import mass_aa, mass_group
import re

mass_aa['u'] = (69.07, 69.021429)  #dehydro-Ala, de-phosphorylated Ser
mass_aa['v'] = (83.10, 83.03708)


def fake_the_tag(sequence):
    """creates a fake amino acid to mimic a TMT or iTRAQ moiety

    Patterns of type 'AA(PTM_codes)' (ex. S(21)) in a peptide sequence are
    converted to lower-case amino acid when phosphorylated. Other PTM CODES are
    (737 or ...) are faked.

    I(737)DEGHSNS(23)SPR  -->  oDEGHSNsSPR

    """
    strings = re.split('[(), ]', sequence)
    result = []
    for astring in strings:
        if astring.isalpha():
            result.append(astring)
        elif astring in ['21', '23']:
            if astring == '21':
                amino_acid = result[-1][-1].lower()
            elif astring == '23':
                amino_acid = result[-1][-1]
                if amino_acid == 'S':
                    amino_acid = 'u'
                elif amino_acid == 'T':
                    amino_acid = 'v'
            result[-1] = result[-1][:-1] + amino_acid

        elif astring == '737':
            mod_aa = result[-1][-1]
            (av, mono) = mass_group['tmt']
            mass_aa['o'] = (mass_aa[mod_aa][0] + av, mass_aa[mod_aa][1] + mono)
            result[-1] = result[-1][:-1] + 'o'
        elif astring == '214':
            mod_aa = result[-1][-1]
            (av, mono) = mass_group['itrq']
            mass_aa['o'] = (mass_aa[mod_aa][0] + av, mass_aa[mod_aa][1] + mono)
            result[-1] = result[-1][:-1] + 'o'

    return ''.join(result)
#
def get_b_series(sequence):
    """b_series"""
    mass = 1.007
    series = []
    for item in sequence:
        try:
            mass += mass_aa[item][1]
        except KeyError:
            if item in 'LI':
                mass += mass_aa['X'][1]
            else:
                print('AA rare ->%s<-' % item)
        
        series.append(mass)
    return series
#
def get_y_series(sequence):
    """y_series"""
    mass = 19.0176
    series = []
    sequence = reversed(list(sequence))
    for item in sequence:
        try:
            mass += mass_aa[item][1]
        except KeyError:
            if item in 'LI':
                mass += mass_aa['X'][1]
            else:
                print('AA rare ->%s<-' % item)
        
        series.append(mass)
    return series

if __name__ == "__main__":
    
    peptide = "ASPVTuXXTWIDsMHYCR"
    peptide = "KLEKEEEEGIsQEuSEEEQ"
    print("y ions   b ions")
    for y, b in zip(get_y_series(peptide), reversed(get_b_series(peptide))):
        print("%7.2f %8.2f" % (y, b))
        
##"ASPVTuXXTWIDsMHYCR"
##y ions   b ions
## 175.12  2123.94
## 278.13  1967.84
## 441.19  1864.83
## 578.25  1701.77
## 709.29  1564.71
## 876.29  1433.67
## 991.32  1266.67
##1104.40  1151.65
##1290.48  1038.56
##1391.53   852.48
##1504.61   751.43
##1617.70   638.35
##1686.72   525.27
##1787.76   456.25
##1886.83   355.20
##1983.89   256.13
##2070.92   159.08
##2141.95    72.04
##Script terminated.